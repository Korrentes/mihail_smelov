import com.sun.org.apache.xerces.internal.impl.xpath.regex.Match;

import static java.lang.System.exit;

/**
 * Created by USER on 09.09.2016.
 */
public class Calculator {



    public double add(double operand_a ,double operand_b){
        return operand_a + operand_b;
    }

    public double diff(double operand_a ,double operand_b){
        return operand_a - operand_b;
    }
    public double mult(double operand_a ,double operand_b){
        return operand_a * operand_b;
    }

    public String div(double operand_a ,double operand_b) {
        if (operand_b == 0) {
            return "Делить на 0 нельзя";
        } else {
            return String.valueOf (operand_a / operand_b);
        }
    }

    public String div(int operand_a , int operand_b) {
        if (operand_b == 0) {
            return "Делить на 0 нельзя";
        } else {
            return String.valueOf (operand_a / operand_b);
        }
    }

    public String balance(double operand_a ,double operand_b){
        if(operand_b == 0){
            return "Делить на 0 нельзя";
        }else {
            double a = Math.floor(operand_a / operand_b);
            double b = operand_a % operand_b;

            int i = (int) a;
            return "Целое: " + i
                    + " Остаток: " + b;
        }
    }

    public double pow(double operand_a ,double operand_b) {

        double result = 1;

        if(operand_b == 1){
            result = operand_a;
        }
        if (operand_b == 0) {
            result = 1;}
        if(operand_b > 0) {

            for (int i = 0; i < operand_b; i++) {

                result = result * operand_a;

            }
        }
        if(operand_b < 0){
            for (int i = 0; i > operand_b; i--) {
                result = result * operand_a;

            }
            System.out.println("Ответ: 1 / " + result);
            exit(1);
        }
        return result;
    }

}

