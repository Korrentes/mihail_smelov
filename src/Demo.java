/**
 * Created by USER on 09.09.2016.
 */
public class Demo {

    public static void main(String[] args){
        Calculator calculator = new Calculator();

        double operand_a = 4.5;
        double operand_b = 2;

        int a = 8;
        int b = 2;

        System.out.println("Сложение: "+ calculator.add(operand_a ,operand_b));

        System.out.println("Вычитание: "+ calculator.diff(operand_a ,operand_b));

        System.out.println("Умножение: " + calculator.mult(operand_a ,operand_b));

        System.out.println("Возведение в степень: " + calculator.pow(operand_a ,operand_b));

        System.out.println("Деление: " + calculator.div(operand_a ,operand_b));

        System.out.println("Целочисленное деление: " + calculator.div(a ,b));

        System.out.println("Деление с остатком: " + calculator.balance(operand_a ,operand_b));
    }
}
